```
You need to have `sibilant` installed.
> npm install -g sibilant
```

(defn sibilant-compile [& forms]
  (def p "compiler process" (os/spawn ["sibilant" "-i"] :p {:in :pipe :out :pipe}))
  (def sin (p :in))
  (def sout (p :out))

  (each form forms
    (:write sin (string/format "%j" form)))
  (:close sin)
  (def output (:read sout :all))
  (assert (= 0 (os/proc-wait p)))
  output)

# (defmacro sibilant-compile [& forms]
#   ~(,_sibilant-compile ,;(map |~',$ forms)))
# |~',$ turns tuple of x into tuple of (quote x)

(defn- main [&]
  (def program ['(console.log "Hello" "World") '(console.log "Hello" "World")])

  (def p (os/spawn ["sibilant" "-i"] :p {:in :pipe :out :pipe}))
  (def sin (p :in))
  (def sout (p :out))

  (each form program
    (:write sin (string/format "%j" form)))
  (:close sin)

  (print (:read sout :all))
  (print (os/proc-wait p)))
