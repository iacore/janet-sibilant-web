(use joy)
(use ./compile)

(defn my-layout [{:body body :request request}]
  (text/html
    (doctype :html5)
    [:html {:lang "en"}
      [:head
      [:title "title"]
      [:meta {:charset "utf-8"}]
      [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
      [:meta {:name "csrf-token" :content (authenticity-token request)}]]
      [:body body]]))

(def target-div-id "changeme")

(defn home [req]
 [[:h1 "Hello world"]
  [:div {:id target-div-id} "JS should overwrite this"]
  [:script {:src "/js-endpoint"}]])

(defn application/javascript [content]
  @{:status 200
    :body content
    :headers @{"Content-Type" "application/javascript"}})

(defn js-endpoint [req]
  (def hello "Hello")
  (def content (sibilant-compile
    ~(console.log ,hello)
    ~(var el (document.getElementById ,target-div-id))
    ~(set el 'innerText "hello from sibilant+Janet")
  ))
  (print content)
  (application/javascript content))


(defroutes routes
  [:get "/" home]
  [:get "/js-endpoint" js-endpoint])

(def app (handler routes))

(def app-with-layout (as-> (handler routes) ?
                            (layout ? my-layout)
                            (not-found ?)))

(defn- main [&]
  (def port 1234)
  (printf "Listening on http://localhost:%d/" port)
  (server app-with-layout port "localhost"))
