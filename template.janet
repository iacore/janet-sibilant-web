"TodoMVC"

#(defmacro varx "Declare reactive variable"
#  [name init]
#  '{:name ,name :value ,init})

(defmacro templatex "Declare reactive template"
  [& forms]
  (def lastidx (- (length forms) 1))
  (when (< lastidx 0)
    (error "<function templatex> expects at least 1 argument"))
  (def template "html template" (forms lastidx))
  (def forms (slice forms 0 lastidx))

  # (pp forms)
  (each form forms
    (pp form))
  ~())

(defn item-list [items]
 '[:div "Items"
   [:ul
    (foreach item (get items)
      [:li (string item)])
   ]
  ]
)

(defn component-template []
(templatex
  (var items ["item0"])
  (var itemtext "")
  
  [:div
    [:input {:bind:value itemtext}]
    [:button {:onclick (fn [evt]
                          (set items [itemtext ;items])
                          (set itemtext ""))} "Add item"]
    ,(item-list items)
  ]))

(defn main [&]
  (printf "Result: %P" (component-template)))
