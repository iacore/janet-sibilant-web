

(def template '[:div {:class "hello"}
 ;(map |[:div (string $)] [1 2 3])
])

(def el [:div {:class "hello"}
 (map (fn [x] [:div (string x)]) [1 2 3])
])

(pp el)

